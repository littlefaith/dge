#include <jni.h>
#include <string>
#include <cstring>
#include "md5.h"
#include <android/log.h>

const char *LOG_TAG = "native-lib";
static char *PACKAGE_NAME = (char *) "com.dge.app";
static char *APP_SIGNATURE = (char *) "308203343082021c020101300d06092a864886f70d01010b05003060310c300a06035504030c036564673110300e060355040b0c07636f6d70616e79310e300c060355040a0c0564756d6d79310e300c06035504070c056a696e616e3111300f06035504080c087368616e646f6e67310b3009060355040613023836301e170d3233313131333134303930365a170d3438313130363134303930365a3060310c300a06035504030c036564673110300e060355040b0c07636f6d70616e79310e300c060355040a0c0564756d6d79310e300c06035504070c056a696e616e3111300f06035504080c087368616e646f6e67310b300906035504061302383630820122300d06092a864886f70d01010105000382010f003082010a0282010100ecfc12cc5971f7ba2af92d265c14677c068719314961843cc6cf0a2727e7f0474ca643c4317681662fff7e104379f57cc4468cd0c2a37d9981a3d06007998420147bc63aac2c892d23104b52805a079dc15b1a1d3506cc422e62258d9b1cc3ec4cce300482ea8fda1a64ddede04932617394fd275336e4d23c7a8365bc7b679ad216181f1223f74616ab573f5be2f55385a1f629a8b1fbe165ab9a94c572b6702506610d488fef6ceb1c793f32fec816e70ed9ea7dd2393eaff70495725bb38723b6dd7d22cacc97115bba85c7e905c4ac7064f0e8b0e7d1a1fff72f5f4bf0598c01f3a4748d42addc3f33facb052e13614a1732d474db96fef3fef2d02e3b2d0203010001300d06092a864886f70d01010b0500038201010039f2581e7b07f3314dc5c9175e11aa0b39fd780d67392401cbe220dd56533cceb7633019d9467058ee2cbb38ca27a3f90dc35dd62aaf37e76214c9e6fd1b0d1f2c5bc6aac65534caea9ccbdb542db1436528aedebf64f90264c890ab9d1e65f16c5fe3019b57db7e2a667de036dc11c2f0c2ba93a42d9ea895187bff31760ed812d4a26bcab98b1948f797dece0b1a2467fa6d2d80185feb05d646faca83896cae107eb0689e27210e21f6214235d3c95d9136dfe01f3c779e99a5820d7697df09c5c45ea64cf955f2e1268cbcc4df3a90b66f72717457a550a276ec88f911916b88ae7263a1119f4f50b699a1e62b0267887dbb98900c3a4c39193358e7f928";
static int is_verify = 0;

using namespace std;

extern "C" {

JNIEXPORT void JNICALL
Java_com_dge_common_NdkMethods_signatureVerify(JNIEnv *env, jclass type, jobject context);
}



extern "C" JNIEXPORT void JNICALL
Java_com_dge_common_NdkMethods_signatureVerify(JNIEnv *env, jclass type,
                                               jobject context) {

    // 获取 PackageManager
    jclass j_clz = env->GetObjectClass(context);
    jmethodID j_mid = env->GetMethodID(j_clz, "getPackageName", "()Ljava/lang/String;");
    jstring j_package_name = (jstring) env->CallObjectMethod(context, j_mid);
    const char *c_pack_name = env->GetStringUTFChars(j_package_name, NULL);
    // 先比较包名是否相等，包名不想等返回
    if (strcmp(c_pack_name, PACKAGE_NAME) != 0) {
        jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
        env->ThrowNew(newExcCls, "包名不符！");
        return;
    }
    j_mid = env->GetMethodID(j_clz, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject pack_manager = env->CallObjectMethod(context, j_mid);
    j_clz = env->GetObjectClass(pack_manager);
    j_mid = env->GetMethodID(j_clz, "getPackageInfo",
                             "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    jobject j_pack_info = env->CallObjectMethod(pack_manager, j_mid, j_package_name, 0x00000040);

    j_clz = env->GetObjectClass(j_pack_info);
    jfieldID j_fid = env->GetFieldID(j_clz, "signatures", "[Landroid/content/pm/Signature;");
    jobjectArray signatures_array = (jobjectArray) env->GetObjectField(j_pack_info, j_fid);
    jobject signature_obj = env->GetObjectArrayElement(signatures_array, 0);

    j_clz = env->GetObjectClass(signature_obj);
    j_mid = env->GetMethodID(j_clz, "toCharsString", "()Ljava/lang/String;");
    jstring j_signature = (jstring) env->CallObjectMethod(signature_obj, j_mid);
    const char *c_signature = env->GetStringUTFChars(j_signature, NULL);
    if (strcmp(APP_SIGNATURE, c_signature) == 0) {
        // 认证成功
        __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "is_verify = 1");
        is_verify = 1;
    } else {
        // 认证失败
        is_verify = 0;
        jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
        // env->ThrowNew(newExcCls, c_signature);
       env->ThrowNew(newExcCls, "签名非法！");
    }
}