package com.dge.common.widgets.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import androidx.core.R
import androidx.core.widget.NestedScrollView
import com.dge.common.other.StretchEdgeEffect

/**
 * @Author: Jasper Jiao
 * @Date: 2022/1/14 14:19
 * @Description: 有android12过度拉伸效果的ScrollView
 */
class StretchScrollView : NestedScrollView {
    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, R.attr.nestedScrollViewStyle)

    @SuppressLint("RestrictedApi", "VisibleForTests")
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) { //android12以下应用这个效果，12及以上NestedScrollView自带
            mEdgeGlowTop = object : StretchEdgeEffect(context, this@StretchScrollView) {
                var oldGlowScaleY = mGlowScaleY
                override fun another(): StretchEdgeEffect {
                    this.mGlowScaleY = oldGlowScaleY
                    return this
                }

                override fun pivotY(): Float {
                    return 0f
                }

                override fun pivotX(): Float {
                    return 0f
                }
            }

            mEdgeGlowBottom = object : StretchEdgeEffect(context, this@StretchScrollView) {
                var oldGlowScaleY = mGlowScaleY
                override fun another(): StretchEdgeEffect {
                    if (this.mGlowScaleY != 0f)
                        oldGlowScaleY = mGlowScaleY
                    this.mGlowScaleY = 0f
                    return this
                }

                override fun pivotY(): Float {
                    return view.height * 1.0f
                }

                override fun pivotX(): Float {
                    return view.width * 1.0f
                }
            }
        }
    }
}