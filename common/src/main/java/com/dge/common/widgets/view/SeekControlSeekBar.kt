package com.dge.common.widgets.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatSeekBar

/**
 * @Author: Jasper Jiao
 * @Date: 2022/1/21 10:40
 * @Description: 控制是否可以进行拖动的SeekBar
 */
class SeekControlSeekBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatSeekBar(context, attrs, defStyleAttr) {
    /**
     * 是否支持拖动进度
     */
    var touch = true

    /**
     * onTouchEvent 是在 SeekBar 继承的抽象类 AbsSeekBar
     */
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return if (touch) {
            super.onTouchEvent(event)
        } else false
    }
}