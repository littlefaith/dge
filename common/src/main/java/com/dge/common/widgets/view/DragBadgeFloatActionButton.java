package com.dge.common.widgets.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.dge.common.utils.SizeConvertUtils;

/**
 * 在DragFloatActionButton的基础上增加了消息个数红点
 */
public class DragBadgeFloatActionButton extends DragFloatActionButton {

    //数量红点是否显示
    private int tipVisibility = View.GONE;
    //数量红点的文字
    private String text = "0";
    Paint paint = new Paint();
    Paint textPaint = new Paint();

    public DragBadgeFloatActionButton(Context context) {
        super(context);
    }

    public DragBadgeFloatActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DragBadgeFloatActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        if (tipVisibility == View.VISIBLE) {
            int radius = SizeConvertUtils.INSTANCE.dp2px(8f);
            paint.setColor(Color.RED);
            paint.setAntiAlias(true);
            paint.setDither(true);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            canvas.drawCircle(width - radius, radius, radius, paint);
            textPaint.setAntiAlias(true);
            textPaint.setStyle(Paint.Style.FILL);
            int textSizePx = SizeConvertUtils.INSTANCE.sp2px(9);
            textPaint.setTextAlign(Paint.Align.CENTER);
            textPaint.setColor(0xffffffff);
            textPaint.setFakeBoldText(false);
            textPaint.setTextSize(textSizePx);
            canvas.drawText(this.text, width - radius, (int) (radius + textSizePx / 3.0f), textPaint);
        }
    }

    public void setCountText(String text) {
        this.text = text;
        invalidate();
    }

    public String getCountText() {
        return text;
    }

    public void setBadgeVisibility(int visibility) {
        tipVisibility = visibility;
        invalidate();
    }
}
