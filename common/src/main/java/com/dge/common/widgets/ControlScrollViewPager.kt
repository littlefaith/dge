package com.dge.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class ControlScrollViewPager : ViewPager {
    var isCanScroll = false
        private set

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (isCanScroll) {
            try {
                super.onInterceptTouchEvent(event)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
                false
            }
        } else false
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return if (isCanScroll) super.onTouchEvent(ev) else false
    }

    fun setScanScroll(isCanScroll: Boolean) {
        this.isCanScroll = isCanScroll
    }

    fun toggleLock() {
        isCanScroll = !isCanScroll
    }
}