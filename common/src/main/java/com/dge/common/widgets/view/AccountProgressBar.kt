package com.dge.common.widgets.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.ProgressBar
import com.dge.common.R
import com.dge.common.extentions.dp
import com.dge.common.extentions.drawable

class AccountProgressBar : ProgressBar {
    //用来显示进度数值的成员
    private var mPaint: Paint? = null
    private var mProcess: String? = null

    constructor(context: Context?) : super(context) {
        initValue()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initValue()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initValue()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        initValue()
    }

    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val process = progress.toFloat()
        val max = max.toFloat()
        val x = (width * (process / max)).toInt()
        val drawable = R.drawable.bg_progress_indicator.drawable()!!
        drawable.setBounds(x, 0, x + 12.dp, 8.dp)
        drawable.draw(canvas)
    }

    // 4. 重写setProgress方法，主要是更新文字变量的数值
    @Synchronized
    override fun setProgress(progress: Int) {
        mProcess = "$progress%"
        super.setProgress(progress)
    }

    fun initValue() {
        mProcess = "0%"
        mPaint = Paint()
        mPaint!!.color = Color.GREEN
        mPaint!!.textSize = 40f
    }
}