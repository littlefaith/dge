package com.dge.common.widgets;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private static final String TAG = "SpacesItemDecoration";
    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private boolean isStaggeredGridLayout;

    public SpacesItemDecoration(int spanCount, int spacing, boolean includeEdge, boolean isStaggeredGridLayout) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.isStaggeredGridLayout = isStaggeredGridLayout;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column;
        if (isStaggeredGridLayout) {
            StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams(); //
            column = params.getSpanIndex() % spanCount; // item column 瀑布流
        } else {
            column = position % spanCount; // item column 网格布局
        }
        if (includeEdge) {
            outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
//            Timber.d( "outRect.left = " + outRect.left + "; outRect.right = " + outRect.right);
            if (position < spanCount) { // top edge
                outRect.top = spacing;
            }
            outRect.bottom = spacing; // item bottom
        } else {
            outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
            outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spacing; // item top
            }
        }
    }
}