package com.dge.common.common

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object URLConstants {
    const val IS_PUBLISH_VERSION = false
    private const val DEBUG_IP = "60.208.63.156"
    private const val RELEASE_IP = "60.208.63.156"
    private const val RELEASE_BASE_URL = "http://$RELEASE_IP"
    private const val DEBUG_BASE_URL = "http://$DEBUG_IP"
    var BASE_URL = if (IS_PUBLISH_VERSION) RELEASE_BASE_URL else DEBUG_BASE_URL
    val URL_USER_BASE = "$BASE_URL:6097"
}