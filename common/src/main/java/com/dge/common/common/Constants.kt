package com.dge.common.common

import io.michaelrocks.paranoid.Obfuscate

@Obfuscate
object Constants {
    @JvmField
    val fileProviderAuthority = "${AppConfig.getPackageName()}.provider"
    const val defaultFuncs =
        "[\"一场仗\",\"目标任务\",\"重点项目\",\"成果晾晒\",\"比学赶超\",\"揭榜挂帅\",\"督办圈阅\",\"超时亮灯\",\"一表通\"]"
    const val defaultStatistics = "{}"
    const val PGY_SHORTCUT = ""
    const val PGY_API_KEY = ""
    const val PGY_APP_KEY = ""
    const val PREF_KEY_TOKEN = "pref_key_token" //token
    const val PREF_KEY_USER_ID = "pref_key_user_id" //用户id
    const val PREF_KEY_USER_NAME = "pref_key_user_name" //用户名称
    const val PREF_KEY_USER_ICON = "pref_key_user_icon" //头像
    const val PREF_KEY_DEV_ID = "pref_key_dev_id" //设备id
    const val PREF_KEY_FUNC_DATA = "pref_key_func_data" //功能菜单
    const val PREF_KEY_STATISTIC_DATA = "pref_key_statistic_data" //统计面板
    const val PREF_KEY_ACCOUNT = "pref_key_account" //用户名
    const val PREF_KEY_PWD = "pref_key_pwd"//密码
    const val PREF_KEY_IS_REMEMBER_LOGIN_INFO = "pref_key_is_remember_login_info" //是否记住登录密码
    const val PREF_KEY_LAST_CANCELED_VERSION_CODE = "pref_key_last_canceled_version_code"//上次取消更新的版本号
    const val PREF_KEY_LAST_UPDATE_TIME_MILLS = "pref_key_last_update_time_mills"//上次弹出更新的时间戳
    const val PREF_KEY_IS_LAST_UPDATE_CANCELLED = "pref_key_is_last_update_cancelled"//是否取消更新
    const val default = "default" //储存是否第一次登录的sp
    const val firstIn = "first" //储存是否第一次登录的sp

    //路由地址
    const val ACTIVITY_URL_LOGIN = "/login/login_register/view/LoginActivity"//登录地址
    const val ACTIVITY_URL_MAIN = "/main/ui/MainActivity"//主界面
    const val ACTIVITY_URL_SPLASH = "/app/ui/SplashActivity"//闪屏界面地址
}