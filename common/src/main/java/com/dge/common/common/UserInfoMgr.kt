package com.dge.common.common

import com.dge.common.extentions.mmkv
import com.tencent.mmkv.MMKV

object UserInfoMgr {
    lateinit var userIcon: String
    lateinit var account: String
    const val TAG = "UserInfoMgr"
    lateinit var userId: String
    lateinit var token: String
    lateinit var userName: String
    lateinit var deviceID: String
    var lastUpdateTimeMills = 0L
    var lastCanceledUpdateVersionCode = 0
    var isLastUpdateCanceled = false
    lateinit var functionData: String
    lateinit var statisticsData: String
    private val defaultMmkv: MMKV = mmkv(Constants.default)
    private val firstInMmkv: MMKV = mmkv(Constants.firstIn)

    fun loadUserInfo() {
        defaultMmkv.apply {
            userId = getString(Constants.PREF_KEY_USER_ID, "").toString()
            account = getString(Constants.PREF_KEY_ACCOUNT, "").toString()
            userName = getString(Constants.PREF_KEY_USER_NAME, "").toString()
            userIcon = getString(Constants.PREF_KEY_USER_ICON, "").toString()
            token = getString(Constants.PREF_KEY_TOKEN, "").toString()
            deviceID = getString(Constants.PREF_KEY_DEV_ID, "").toString()
        }
        firstInMmkv.apply {
            lastUpdateTimeMills = getLong(Constants.PREF_KEY_LAST_UPDATE_TIME_MILLS, System.currentTimeMillis())
            lastCanceledUpdateVersionCode = getInt(Constants.PREF_KEY_LAST_CANCELED_VERSION_CODE, 0)
            isLastUpdateCanceled = getBoolean(Constants.PREF_KEY_IS_LAST_UPDATE_CANCELLED, false)
            functionData = getString(Constants.PREF_KEY_FUNC_DATA, Constants.defaultFuncs).toString()
            statisticsData = getString(Constants.PREF_KEY_STATISTIC_DATA, Constants.defaultStatistics).toString()
        }
    }

    fun saveUserInfo() {
        defaultMmkv.apply {
            encode(Constants.PREF_KEY_USER_ID, userId)
            encode(Constants.PREF_KEY_ACCOUNT, account)
            encode(Constants.PREF_KEY_DEV_ID, deviceID)
            encode(Constants.PREF_KEY_USER_NAME, userName)
            encode(Constants.PREF_KEY_USER_ICON, userIcon)
            encode(Constants.PREF_KEY_TOKEN, token)
        }
        firstInMmkv.apply {
            encode(Constants.PREF_KEY_LAST_UPDATE_TIME_MILLS, lastUpdateTimeMills)
            encode(Constants.PREF_KEY_LAST_CANCELED_VERSION_CODE, lastCanceledUpdateVersionCode)
            encode(Constants.PREF_KEY_IS_LAST_UPDATE_CANCELLED, isLastUpdateCanceled)
            encode(Constants.PREF_KEY_FUNC_DATA, functionData)
            encode(Constants.PREF_KEY_STATISTIC_DATA, statisticsData)
        }
    }

    fun saveUserInfo(name: String) {
        if (name == Constants.firstIn)
            firstInMmkv.apply {
                encode(Constants.PREF_KEY_LAST_UPDATE_TIME_MILLS, lastUpdateTimeMills)
                encode(Constants.PREF_KEY_LAST_CANCELED_VERSION_CODE, lastCanceledUpdateVersionCode)
                encode(Constants.PREF_KEY_IS_LAST_UPDATE_CANCELLED, isLastUpdateCanceled)
                encode(Constants.PREF_KEY_FUNC_DATA, functionData)
                encode(Constants.PREF_KEY_STATISTIC_DATA, statisticsData)
            }
        if (name == Constants.default)
            defaultMmkv.apply {
                encode(Constants.PREF_KEY_USER_ID, userId)
                encode(Constants.PREF_KEY_ACCOUNT, account)
                encode(Constants.PREF_KEY_DEV_ID, deviceID)
                encode(Constants.PREF_KEY_USER_NAME, userName)
                encode(Constants.PREF_KEY_USER_ICON, userIcon)
                encode(Constants.PREF_KEY_TOKEN, token)
            }
    }

    private fun clearUserInfo() {
        defaultMmkv.clear()
    }

    fun logout() {
        userId = "0"
        account = ""
        deviceID = ""
        userName = ""
        userIcon = ""
        token = ""
        functionData = ""
        statisticsData = ""
        clearUserInfo()
    }
}