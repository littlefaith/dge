package com.dge.common.common

import android.os.Build
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.RomUtils
import com.dge.common.BuildConfig
import com.dge.common.LibApp
import com.dge.common.utils.Utils

object AppConfig {
    //是否允许抓包
    var allowNetProxy = false

    /**
     * 当前是否为 Debug 模式
     */
    fun isDebug(): Boolean {
        return BuildConfig.DEBUG || Utils.isApkInDebug(LibApp.context)
    }

    /**
     * 深色模式的值为:0x21
     * 浅色模式的值为:0x11
     */
    fun isDarkTheme(): Boolean {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q || RomUtils.isMeizu()) && LibApp.context.resources.configuration.uiMode == 0x21
    }

    /**
     * 当前是否为发布release版本
     */
    fun isPublishRelease(): Boolean {
        return !BuildConfig.DEBUG && URLConstants.IS_PUBLISH_VERSION && !Utils.isApkInDebug(LibApp.context)
    }

    /**
     * 获取当前应用的包名
     */
    fun getPackageName(): String {
        return AppUtils.getAppPackageName()
    }

    /**
     * 获取当前应用的版本名
     */
    fun getVersionName(): String {
        return AppUtils.getAppVersionName()
    }

    /**
     * 获取当前应用的版本码
     */
    fun getVersionCode(): Int {
        return AppUtils.getAppVersionCode()
    }
//    /**
//     * 获取当前应用的渠道名
//     */
//    fun getProductFlavors(): String? {
//        return BuildConfig.FLAVOR
//    }
}