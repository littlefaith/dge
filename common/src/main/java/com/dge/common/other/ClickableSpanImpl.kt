package com.dge.common.other

import android.text.NoCopySpan
import android.text.style.ClickableSpan
import android.view.View

/**
 * @time 2023/1/29 16:07
 * @des 防止ClickableSpan内存泄露
 */
open class ClickableSpanImpl(private val clickAction: () -> Unit) : ClickableSpan(), NoCopySpan {
    override fun onClick(widget: View) {
        clickAction()
    }
}