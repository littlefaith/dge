package com.dge.common.other

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import com.hjq.toast.style.BlackToastStyle
import com.dge.common.extentions.dp
import com.dge.common.extentions.sp

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject-Kotlin
 *    time   : 2021/02/27
 *    desc   : Toast 样式配置
 */
open class ToastStyle : BlackToastStyle() {
    override fun getBackgroundDrawable(context: Context): Drawable {
        val drawable = GradientDrawable()
        // 设置颜色
        drawable.setColor(-0x78000000)
        // 设置圆角
        drawable.cornerRadius = 999.dp.toFloat()
        return drawable
    }

    override fun getTextSize(context: Context): Float {
        return 14f.sp
    }

    override fun getHorizontalPadding(context: Context): Int {
        return 24.sp
    }

    override fun getVerticalPadding(context: Context): Int {
        return 16.sp
    }
}