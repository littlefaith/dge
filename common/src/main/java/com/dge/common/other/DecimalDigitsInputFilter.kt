package com.dge.common.other

import android.text.InputFilter
import android.text.Spanned

/**
 * 限制输入小数点位数，以及开头不允许输入
 */
class DecimalDigitsInputFilter(
    private val decimalDigits: Int //限制小数位数
) : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        var dotPos = -1
        val len = dest.length
        for (i in 0 until len) {
            val c = dest[i]
            if (c == '.' || c == ',') {
                dotPos = i
                break
            }
        }
        if (source == "." && dstart == 0 && dend == 0) {
            return ""
        }
        if (dotPos >= 0) {
            // protects against many dots
            if (source == "." || source == ",") {
                return ""
            }
            // if the text is entered before the dot
            if (dend <= dotPos) {
                return null
            }
            if (len - dotPos > decimalDigits) {
                return ""
            }
        }
        return null
    }
}