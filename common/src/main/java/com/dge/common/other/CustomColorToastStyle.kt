package com.dge.common.other

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.annotation.ColorRes
import com.dge.common.extentions.color
import com.dge.common.extentions.dp

class CustomColorToastStyle(@ColorRes private val bgColor: Int, @ColorRes private val textColor: Int) : ToastStyle() {
    override fun getTextColor(context: Context?): Int {
        return textColor.color()
    }

    override fun getBackgroundDrawable(context: Context): Drawable {
        val drawable = GradientDrawable()
        // 设置颜色
        drawable.setColor(bgColor.color())
        // 设置圆角
        drawable.cornerRadius = 999.dp.toFloat()
        return drawable
    }
}