package com.dge.common.other

import android.os.Build
import com.lxj.xpopup.animator.PopupAnimator

/**
 * @Description: ios dialog 类型的动画
 * @Date: 2022/1/4 10:57
 * @Author: Jasper Jiao
 */
class IOSStyleDialogAnimator : PopupAnimator() {
    override fun initAnimator() {
        targetView.scaleX = 1.1f
        targetView.scaleY = 1.1f
        targetView.alpha = 0f
    }

    override fun animateShow() {
        val animator = targetView.animate().scaleX(1f).scaleY(1f).alpha(1f)
            .setDuration(300)
        // .setInterpolator(FastOutSlowInInterpolator())
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {//在部分6.0系统会引起crash，这里判断一下
            animator.withLayer() //开启硬件加速
        }
        animator.start()
        // targetView.animation = AnimationUtils.loadAnimation(targetView.context, R.anim.window_ios_in);
    }

    override fun animateDismiss() {
        if (animating) return
        val animator = observerAnimator(
            targetView.animate().scaleX(1.1f).scaleY(1.1f).alpha(0f)
                .setDuration(300)
            // .setInterpolator(FastOutSlowInInterpolator())
        )
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {//在部分6.0系统会引起crash，这里判断一下
            animator.withLayer() //开启硬件加速
        }
        animator.start()
        // targetView.animation = AnimationUtils.loadAnimation(targetView.context, R.anim.window_ios_out);
    }
}