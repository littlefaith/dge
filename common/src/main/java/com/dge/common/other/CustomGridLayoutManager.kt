package com.dge.common.other

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Synopsis     解决SwipeRefreshLayout 与RecyclerView滑动产生冲突，导致的崩溃
 * java.lang.IndexOutOfBoundsException: Inconsistency detected. Invalid item position 12(offset:12).state:21
 *
 * @author Jasper Jiao
 */
class CustomGridLayoutManager : GridLayoutManager {
    constructor(context: Context?, spanCount: Int) : super(context, spanCount)
    constructor(
        context: Context?, spanCount: Int,
        @RecyclerView.Orientation orientation: Int, reverseLayout: Boolean
    ) : super(context, spanCount, orientation, reverseLayout)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )

    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 纵向
     *
     * @param dy
     * @param recycler
     * @param state
     * @return
     */
    override fun scrollVerticallyBy(dy: Int, recycler: RecyclerView.Recycler, state: RecyclerView.State): Int {
        var i = 0
        try {
            i = super.scrollVerticallyBy(dy, recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        return i
    }

    /**
     * 横向
     *
     * @param dx
     * @param recycler
     * @param state
     * @return
     */
    override fun scrollHorizontallyBy(dx: Int, recycler: RecyclerView.Recycler, state: RecyclerView.State): Int {
        val i = 0
        try {
            super.scrollHorizontallyBy(dx, recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        return i
    }
}