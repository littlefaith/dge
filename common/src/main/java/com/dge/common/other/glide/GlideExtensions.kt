package com.dge.common.other.glide

import android.annotation.SuppressLint
import com.bumptech.glide.annotation.GlideExtension
import com.bumptech.glide.annotation.GlideOption
import com.bumptech.glide.request.BaseRequestOptions
import com.dge.common.R

@GlideExtension
object GlideExtensions {
    var placeHolder = R.mipmap.logo

    /**
     * 头像配置
     * @param options
     * @param size 图片内存大小
     * @return
     */
    @SuppressLint("CheckResult")
    @JvmStatic
    @GlideOption
    fun applyAvatar(options: BaseRequestOptions<*>, size: Int = 0): BaseRequestOptions<*> {
        return options.placeholder(placeHolder)
            .error(placeHolder)
            .apply {
                if (size != 0) {
                    override(size)
                }
            }
    }
    //
    // @JvmStatic
    // @GlideOption
    // fun applyInto(options: BaseRequestOptions<*>, imageView: ImageView) {
    //     (options as GlideRequest<File>).into(object : SimpleTarget<File>() {
    //         override fun onResourceReady(resource: File, transition: Transition<in File>?) {
    //             val length = resource.length()
    //             // Timber.d("resource.length()  = %s", resource.length())
    //             if (length < 4500) { //旧版默认图片不清晰，使用onError加载清晰的logo
    //                 imageView.setImageResource(placeHolder)
    //             } else {
    //                 Glide.with(imageView).load(resource).into(imageView)
    //             }
    //         }
    //     })
    // }
}