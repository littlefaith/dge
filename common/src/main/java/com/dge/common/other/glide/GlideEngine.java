package com.dge.common.other.glide;

import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.luck.picture.lib.engine.ImageEngine;
import com.luck.picture.lib.utils.ActivityCompatHelper;

/**
 * @author luck
 * @time 2019-11-13 17:02
 * @des Glide加载引擎
 */
public class GlideEngine implements ImageEngine {

    /**
     * 加载图片
     *
     * @param context   上下文
     * @param url       资源url
     * @param imageView 图片承载控件
     */
    @Override
    public void loadImage(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return;
        }
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(url.startsWith("http") ? DiskCacheStrategy.DATA : DiskCacheStrategy.NONE) //先缓存到本地，再加载，防止预览时图片不是原图
                .into(imageView);
    }

    /**
     * 加载指定url
     *
     * @param context   上下文
     * @param url       资源url
     * @param maxWidth  资源最大加载尺寸
     * @param maxHeight 资源最大加载尺寸
     */
    @Override
    public void loadImage(Context context, ImageView imageView, String url, int maxWidth, int maxHeight) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return;
        }
        Glide.with(context)
                .load(url)
                .override(maxWidth, maxHeight)
                .centerCrop()
                .diskCacheStrategy(url.startsWith("http") ? DiskCacheStrategy.DATA : DiskCacheStrategy.NONE)
                .placeholder(com.luck.picture.lib.R.drawable.ps_image_placeholder)
                .into(imageView);
    }

    /**
     * 加载相册目录封面
     *
     * @param context   上下文
     * @param url       图片路径
     * @param imageView 承载图片ImageView
     */
    @Override
    public void loadAlbumCover(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return;
        }
        Glide.with(context)
                .asBitmap()
                .load(url)
                .override(180, 180)
                .sizeMultiplier(0.5f)
                .diskCacheStrategy(url.startsWith("http") ? DiskCacheStrategy.DATA : DiskCacheStrategy.NONE)
                .transform(new CenterCrop(), new RoundedCorners(8))
                .placeholder(com.luck.picture.lib.R.drawable.ps_image_placeholder)
                .into(imageView);
    }

    /**
     * 加载图片列表图片
     *
     * @param context   上下文
     * @param url       图片路径
     * @param imageView 承载图片ImageView
     */
    @Override
    public void loadGridImage(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return;
        }
        Glide.with(context)
                .load(url)
                .override(200, 200)
                .centerCrop()
                .diskCacheStrategy(url.startsWith("http") ? DiskCacheStrategy.DATA : DiskCacheStrategy.NONE)
                .placeholder(com.luck.picture.lib.R.drawable.ps_image_placeholder)
                .into(imageView);
    }

    @Override
    public void pauseRequests(Context context) {
        Glide.with(context).pauseRequests();
    }

    @Override
    public void resumeRequests(Context context) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return;
        }
        Glide.with(context).resumeRequests();
    }

    private static final class InstanceHolder {
        private static final GlideEngine instance = new GlideEngine();
    }

    public static GlideEngine createGlideEngine() {
        return GlideEngine.InstanceHolder.instance;
    }
}
