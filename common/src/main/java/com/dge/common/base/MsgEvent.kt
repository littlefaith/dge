package com.dge.common.base

class MsgEvent<T> {
    var code: Int
    var data: T? = null
        private set

    constructor(code: Int) {
        this.code = code
    }

    constructor(code: Int, data: T) {
        this.code = code
        this.data = data
    }

    fun setData(data: T) {
        this.data = data
    }

    companion object {
        const val CODE_OUT_LINE = 0X01 //离线了
        const val CODE_LOGIN_EXPIRED = 0X02 //登录已过期
    }
}