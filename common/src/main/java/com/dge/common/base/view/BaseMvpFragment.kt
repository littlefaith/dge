package com.dge.common.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.dge.common.proxy.MvpProxyFragment

/**
 * <pre>
 *     author : dge
 *     e-mail : 229605030@qq.com
 *     time   : 2021/01/30
 *     desc   :
 * </pre>
 */
class BaseMvpFragment<VB : ViewBinding> : BaseFragment<VB>() {
    lateinit var proxyFragment: MvpProxyFragment
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        proxyFragment = createProxyActivity()
        proxyFragment.bindPresenter()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun createProxyActivity(): MvpProxyFragment {
        return if (!::proxyFragment.isInitialized) {
            MvpProxyFragment(this)
        } else proxyFragment
    }

    override fun onDestroyView() {
        proxyFragment.unbindPresenter()
        super.onDestroyView()
    }
}