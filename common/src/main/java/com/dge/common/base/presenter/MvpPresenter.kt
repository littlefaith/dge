package com.dge.common.base.presenter

import com.dge.common.base.view.MvpView

interface MvpPresenter {
    fun attachView(view: MvpView)

    fun detachView()

    fun isAttached(): Boolean
}