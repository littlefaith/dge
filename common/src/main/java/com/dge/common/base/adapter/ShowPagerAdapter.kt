package com.dge.common.base.adapter

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ShowPagerAdapter(
    fm: FragmentManager?,
    behavior: Int,
    private val fragments: ArrayList<Fragment> //在构造函数前边加上val 直接定义这个变量
) : FragmentPagerAdapter(fm!!, behavior) {
    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun saveState(): Parcelable? {
        val bundle = super.saveState() as? Bundle
        bundle?.let {
            it.putParcelableArray("state", null)
        }
        return bundle
    }
//    override fun getPageTitle(position: Int): CharSequence? {
//        return indicatorText[position]
//    }
}
