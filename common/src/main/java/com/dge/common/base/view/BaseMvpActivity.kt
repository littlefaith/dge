package com.dge.common.base.view

import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.dge.common.proxy.MvpProxyActivity

/**
 * <pre>
 *     author : dge
 *     e-mail : 229605030@qq.com
 *     time   : 2021/01/30
 *     desc   :
 * </pre>
 */
abstract class BaseMvpActivity<VB : ViewBinding> : BaseActivity<VB>() {
    lateinit var proxyActivity: MvpProxyActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        proxyActivity = createProxyActivity()
        proxyActivity.bindPresenter()
    }

    private fun createProxyActivity(): MvpProxyActivity {
        return if (!::proxyActivity.isInitialized) {
            MvpProxyActivity(this)
        } else proxyActivity
    }

    override fun onDestroy() {
        // 在activity销毁时，解绑activity和presenter
        proxyActivity.unbindPresenter()
        super.onDestroy()
    }
}