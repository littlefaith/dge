package com.dge.common.base.view

interface MvpView {
    fun showLoading()
    fun showLoading(msg: String)
    fun hideLoading()
}