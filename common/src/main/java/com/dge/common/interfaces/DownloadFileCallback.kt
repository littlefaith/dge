package com.dge.common.interfaces

import java.io.File

interface DownloadFileCallback {
    /**
     * 响应进度更新
     */
    fun onProgress(total: Long, current: Long)
    fun successCallBack(file: File)
    fun failedCallBack(msg: String)
}