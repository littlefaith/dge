package com.dge.common.interfaces

interface OkHttpCallback<T> {
    fun onSuccess(bean: T)
    fun onError(code: Int, msg: String)
    fun onFailure(e: Exception)
}