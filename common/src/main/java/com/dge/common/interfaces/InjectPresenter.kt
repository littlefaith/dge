package com.dge.common.interfaces

import androidx.annotation.Keep

/**
 * <pre>
 *     author : dge
 *     e-mail : 229605030@qq.com
 *     time   : 2021/01/30
 *     desc   :
 * </pre>
 */
@Keep
@Retention
@Target(AnnotationTarget.FIELD)
annotation class InjectPresenter()
