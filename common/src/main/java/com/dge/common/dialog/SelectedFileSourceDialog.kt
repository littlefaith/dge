package com.dge.common.dialog

import android.content.Context
import com.dge.common.R
import com.dge.common.databinding.DialogSelectFileSourceBinding
import com.dge.common.extentions.gone
import com.dge.common.extentions.singleClick
import com.lxj.xpopup.core.BottomPopupView

class SelectedFileSourceDialog(
    context: Context,
    private val selectPhoto: (() -> Unit)? = null,
    private val selectVideo: (() -> Unit)? = null,
    private val selectFile: (() -> Unit)? = null,
    private val onDismissWithSelectNull: (() -> Unit)
) :
    BottomPopupView(context) {
    private var _binding: DialogSelectFileSourceBinding? = null
    private val binding get() = _binding!!
    private var isSelectItems = false
    override fun onCreate() {
        _binding = DialogSelectFileSourceBinding.bind(popupImplView)
        if (selectPhoto == null) {
            binding.tvPicture.gone()
            binding.divider1.gone()
        } else {
            binding.tvPicture.singleClick {
                selectPhoto!!()
                isSelectItems = true
                dismiss()
            }
        }
        if (selectVideo == null) {
            binding.tvVideo.gone()
            binding.divider2.gone()
        } else {
            binding.tvVideo.singleClick {
                selectVideo!!()
                isSelectItems = true
                dismiss()
            }
        }
        if (selectFile == null) {
            binding.tvFile.gone()
        } else {
            binding.tvFile.singleClick {
                selectFile!!()
                isSelectItems = true
                dismiss()
            }
        }
        binding.tvCancelChoose.singleClick {
            dismiss()
        }
    }

    override fun onDismiss() {
        if (!isSelectItems) onDismissWithSelectNull()
    }

    override fun getImplLayoutId(): Int {
        return R.layout.dialog_select_file_source
    }
}