package com.dge.common.dialog

import android.content.Context
import android.widget.TextView
import com.dge.common.R
import com.dge.common.databinding.DialogChoosePicBinding
import com.dge.common.extentions.singleClick
import com.lxj.xpopup.core.BottomPopupView

class SelectedPhotoSourceDialog(context: Context, val takePhoto: () -> Unit, val chooseFromPhotoAlbum: () -> Unit) :
    BottomPopupView(context) {
    private var _binding: DialogChoosePicBinding? = null
    private val binding get() = _binding!!
    override fun onCreate() {
        _binding = DialogChoosePicBinding.bind(popupImplView)
        binding.tvTakePicture.singleClick {
            takePhoto()
            dismiss()
        }
        binding.tvChooseFromPhotoAlbum.singleClick {
            chooseFromPhotoAlbum()
            dismiss()
        }
        binding.tvCancelChoose.singleClick {
            dismiss()
        }
    }

    override fun getImplLayoutId(): Int {
        return R.layout.dialog_choose_pic
    }
}