package com.dge.common;

import android.content.Context;

public class NdkMethods {
    static {
        System.loadLibrary("native-lib");
    }

    public static native void signatureVerify(Context context);
}
