package com.dge.common.extentions

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.dge.common.LibApp

/**
 * Description: 资源操作相关
 * Create by dance, at 2018/12/11
 */
fun Context.color(id: Int) = resources.getColor(id)

fun Context.string(id: Int) = resources.getString(id)

fun Context.stringArray(id: Int): Array<String> = resources.getStringArray(id)

fun Context.drawable(id: Int): Drawable = resources.getDrawable(id)

fun Context.dimenPx(id: Int) = resources.getDimensionPixelSize(id)

fun Int.dimenPx() = LibApp.context.dimenPx(this)

fun Int.color() = ContextCompat.getColor(LibApp.context, this)

fun Int.drawable() = ActivityCompat.getDrawable(LibApp.context, this)

fun Int.string() = LibApp.context.string(this)

fun Int.stringArray() = LibApp.context.stringArray(this)

fun View.color(id: Int) = context.color(id)

fun View.string(id: Int) = context.string(id)

fun View.stringArray(id: Int) = context.stringArray(id)

fun View.drawable(id: Int) = context.drawable(id)

fun View.dimenPx(id: Int) = context.dimenPx(id)

fun Fragment.color(id: Int) = context!!.color(id)

fun Fragment.string(id: Int) = context!!.string(id)

fun Fragment.stringArray(id: Int) = context!!.stringArray(id)

fun Fragment.drawable(id: Int) = context!!.drawable(id)

fun Fragment.dimenPx(id: Int) = context!!.dimenPx(id)

fun RecyclerView.ViewHolder.color(id: Int) = itemView.color(id)

fun RecyclerView.ViewHolder.string(id: Int) = itemView.string(id)

fun RecyclerView.ViewHolder.stringArray(id: Int) = itemView.stringArray(id)

fun RecyclerView.ViewHolder.drawable(id: Int) = itemView.drawable(id)

fun RecyclerView.ViewHolder.dimenPx(id: Int) = itemView.dimenPx(id)

fun Context.createColorStateList(tintColorResource: Int): ColorStateList {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        return resources.getColorStateList(tintColorResource, theme)
    } else {
        @Suppress("DEPRECATION")
        return resources.getColorStateList(tintColorResource)
    }
}