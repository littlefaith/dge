package com.dge.common.extentions

import com.blankj.utilcode.util.EncodeUtils
import java.nio.charset.Charset

/**
 * 解析url的查询参数
 */
fun String.parseQueryParams(): Map<String, String> {
    var index = lastIndexOf("?") + 1
    var queryParam = hashMapOf<String, String>()
    if (index > 0) {
        var query = substring(index, length)
        query.split("&").forEach {
            if (it.contains("=")) {
                var arr = it.split("=")
                if (arr.size > 1) {
                    queryParam[arr[0]] = arr[1]
                }
            }
        }
    }
    return queryParam
}

fun CharSequence?.isValid(): Boolean = this != null && this.toString().isValid()
fun CharSequence?.isNotValid(): Boolean = !isValid()

fun String?.isValid(): Boolean = this != null && !this.equals("null", true) && this.trim().isNotEmpty()
fun String?.isNotValid(): Boolean = !isValid()

fun String.encodeToBase64(charSet: Charset = Charsets.UTF_8): String {
    return EncodeUtils.base64Encode2String(this.toByteArray(charSet))
}

fun String.decodeBase64(charSet: Charset = Charsets.UTF_8): String {
    return String(EncodeUtils.base64Decode(this), charSet)
}