package com.dge.common.aop

/**
 * 网络检测注解
 */
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
annotation class CheckNet constructor(val blockMethod: Boolean = true, val msg: String = "")