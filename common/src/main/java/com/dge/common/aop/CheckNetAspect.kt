package com.dge.common.aop

import android.app.Application
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.core.content.ContextCompat
import com.hjq.toast.ToastUtils
import com.dge.common.manager.ActivityStackManager
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut

/**
 * 网络检测切面
 */
@Suppress("unused")
@Aspect
class CheckNetAspect {
    /**
     * 方法切入点
     */
    @Pointcut("execution(@com.dge.common.aop.CheckNet * *(..))")
    fun method() {
    }

    /**
     * 在连接点进行方法替换
     */
    @Around("method() && @annotation(checkNet)")
    @Throws(Throwable::class)
    fun aroundJoinPoint(joinPoint: ProceedingJoinPoint, checkNet: CheckNet) {
        val application: Application = ActivityStackManager.getInstance().getApplication()
        val manager: ConnectivityManager? = ContextCompat.getSystemService(application, ConnectivityManager::class.java)
        if (manager != null) {
            val info: NetworkInfo? = manager.activeNetworkInfo
            // 判断网络是否连接
            if (info == null || !info.isConnected) {
                ToastUtils.show(if (checkNet.msg.isEmpty()) "当前没有网络连接，请检查网络设置" else checkNet.msg)
                if (checkNet.blockMethod) return
            }
        }
        //执行原方法
        joinPoint.proceed()
    }
}