package com.dge.common.aop

/**
 * 权限申请注解
 */
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
annotation class Permissions constructor(
    val needGrantMode: Int = PermissionsAspect.NEED_GRANT_ALL,
    /**
     * 需要申请权限的集合
     */
    vararg val value: String
)