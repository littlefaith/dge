package com.dge.common

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.app.NotificationManager
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.view.Gravity
import com.blankj.utilcode.util.ProcessUtils
import com.dge.common.common.AppConfig
import com.dge.common.extentions.dp
import com.dge.common.manager.ActivityStackManager
import com.dge.common.other.DebugLoggerTree
import com.dge.common.other.ToastLogInterceptor
import com.dge.common.other.ToastStyle
import com.dge.common.utils.Utils
import com.hjq.toast.ToastUtils
import com.hjq.toast.style.LocationToastStyle
import com.lxj.xpopup.XPopup
import me.jessyan.autosize.AutoSize
import me.jessyan.autosize.AutoSizeConfig
import me.jessyan.autosize.onAdaptListener
import timber.log.Timber

@SuppressLint("StaticFieldLeak")
open class LibApp : Application() {
    companion object {
        @JvmStatic
        lateinit var context: Context
        lateinit var app: LibApp
    }

    protected var isMainProcess: Boolean = false
    var isDebug = true

    override fun onCreate() {
        super.onCreate()
        app = this
        context = applicationContext
        isDebug = AppConfig.isDebug()
        isMainProcess = ProcessUtils.isMainProcess()
        if (isMainProcess) {
            initInMainProcess()
            initAutoSize()
        }
    }

    //在子类中调用，减少isMainProcess调用，减少耗时
    private fun initInMainProcess() {
        com.blankj.utilcode.util.Utils.init(this)
        if (isDebug) {
            Timber.plant(DebugLoggerTree())
        }
        // TheRouter.isDebug = isDebug
        XPopup.setPrimaryColor(resources.getColor(R.color.colorAccent))
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (AppConfig.isDebug()) {
            Timber.d("initInMainProcess cancelAll notification")
        }
        notificationManager.cancelAll()
    }

    /**
     * 初始化ToastUtils
     */
    protected fun initToast() {
        // 初始化吐司
        ToastUtils.init(this, LocationToastStyle(ToastStyle(), Gravity.BOTTOM, 0, 34.dp, 0f, 0f))
        // ToastUtils.init(this, ToastStyle())
        // 设置调试模式
        ToastUtils.setDebugMode(AppConfig.isDebug())
        // 设置 Toast 拦截器
        if (AppConfig.isDebug()) {
            ToastUtils.setInterceptor(ToastLogInterceptor())
        }
    }

    private fun initAutoSize() {
        AutoSizeConfig.getInstance().onAdaptListener = object : onAdaptListener {
            override fun onAdaptBefore(target: Any?, activity: Activity?) {
                setAutoSizeWidth()
            }

            override fun onAdaptAfter(target: Any?, activity: Activity?) {
            }
        }
    }

    private fun setAutoSizeWidth() {
        val instance = AutoSizeConfig.getInstance()
        val width = instance.screenWidth
        val height = instance.screenHeight
        val screenMin = minOf(width, height)
        val screenMax = maxOf(width, height)
        if (AppConfig.isDebug()) {
            Timber.i("setAutoSizeWidth height = $height, width = $width")
        }
        if (screenMin > 0 && (screenMin != width || screenMax != height)) {
            instance.screenWidth = screenMin
            instance.screenHeight = screenMax
            val topActivity = ActivityStackManager.getInstance().getTopActivity()
            if (topActivity != null) {
                AutoSize.autoConvertDensityOfGlobal(topActivity)
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        if (Utils.FontCompatUtils.shouldChangeFontScale(newConfig)) getResources()
        super.onConfigurationChanged(newConfig)
        setAutoSizeWidth()
    }

    override fun getResources(): Resources {
        return Utils.FontCompatUtils.getResources(super.getResources())
    }
}