package com.dge.common.proxy

/**
 * <pre>
 *     author : dge
 *     e-mail : 229605030@qq.com
 *     time   : 2021/01/30
 *     desc   :
 * </pre>
 */
interface IMvpProxy {
    fun bindPresenter()

    fun unbindPresenter()
}