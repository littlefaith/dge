package com.dge.common.proxy

import com.dge.common.base.view.MvpView

/**
 * <pre>
 *     author : dge
 *     e-mail : 229605030@qq.com
 *     time   : 2021/01/30
 *     desc   :
 * </pre>
 */
class MvpProxyActivity(view: MvpView) : MvpProxyImpl(view) {
}