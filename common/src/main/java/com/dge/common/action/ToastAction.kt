package com.dge.common.action

import androidx.annotation.StringRes
import com.hjq.toast.ToastUtils
import com.dge.common.R

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject-Kotlin
 *    time   : 2019/12/08
 *    desc   : 吐司意图
 */
interface ToastAction {
    fun toast(text: CharSequence?) {
        ToastUtils.show(text)
    }

    fun toast(@StringRes id: Int) {
        ToastUtils.show(id)
    }

    fun toast(`object`: Any?) {
        ToastUtils.show(`object`)
    }

    fun toastNetFailure() {
        ToastUtils.show(R.string.net_failure)
    }
}