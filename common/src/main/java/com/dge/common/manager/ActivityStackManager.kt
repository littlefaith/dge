package com.dge.common.manager

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.os.Bundle
import android.util.Log
import androidx.collection.ArrayMap
import com.dge.common.common.AppConfig
import com.dge.common.extentions.TAG

@SuppressLint("LogNotTimber")
class ActivityStackManager private constructor() : ActivityLifecycleCallbacks {
    companion object {
        const val TAG = "ActivityStackManager"
        const val STATUS_CREATED = 0
        const val STATUS_STARTED = 1
        const val STATUS_RESUMED = 2
        const val STATUS_PAUSED = 3
        const val STATUS_STOPPED = 4
        const val STATUS_FINISHING = 5
        const val STATUS_DESTROYED = 6

        @Suppress("StaticFieldLeak")
        private val sInstance: ActivityStackManager by lazy { ActivityStackManager() }

        @JvmStatic
        fun getInstance(): ActivityStackManager {
            return sInstance
        }

        /**
         * 获取一个对象的独立无二的标记
         */
        private fun getObjectTag(`object`: Any): String {
            // 对象所在的包名 + 对象的内存地址
            return `object`.javaClass.name + Integer.toHexString(`object`.hashCode())
        }
    }

    /** Activity 存放集合 */
    private val activitySet: ArrayMap<String?, Activity?> = ArrayMap()

    /** 应用生命周期回调 */
    private val lifecycleCallbacks: ArrayList<ApplicationLifecycleCallback> = ArrayList()

    /** 当前应用上下文对象 */
    private lateinit var application: Application

    /** 栈顶的 Activity 对象 */
    private var topActivity: Activity? = null

    /** 前台并且可见的 Activity 对象 */
    private var resumedActivity: Activity? = null
    var callBack: ((activity: Activity, status: Int) -> Unit)? = null

    fun init(
        application: Application,
        callBack: ((activity: Activity, status: Int) -> Unit)? = null
    ): ActivityStackManager {
        this.application = application
        this.application.registerActivityLifecycleCallbacks(this)
        this.callBack = callBack
        return this
    }

    /**
     * 获取 Application 对象
     */
    fun getApplication(): Application {
        return application
    }

    /**
     * 获取栈顶的 Activity
     */
    fun getTopActivity(): Activity? {
        return topActivity
    }

    /**
     * 获取栈二的 Activity
     */
    fun getSecondActivity(): Activity? {
        if (AppConfig.isDebug()) Log.d(TAG, "getSecondActivity mActivitySet.size() = ${activitySet.size}")
        return if (activitySet.size >= 2) {
            activitySet[activitySet.keyAt(activitySet.size - 2)]
        } else {
            getTopActivity()
        }
    }

    /**
     * 获取栈3的 Activity
     */
    fun getThirdActivity(): Activity? {
        Log.d(TAG, "getThirdActivity mActivitySet.size() = " + activitySet.size)
        return if (activitySet.size >= 3) {
            activitySet[activitySet.keyAt(activitySet.size - 3)]
        } else {
            getSecondActivity()
        }
    }

    /**
     * 获取栈底部的Activity
     */
    fun getBottomActivity(): Activity? {
        if (AppConfig.isDebug()) Log.d(TAG, "getBottomActivity mActivitySet.size() = " + activitySet.size)
        return if (activitySet.size > 0) {
            activitySet[activitySet.keyAt(0)]
        } else {
            getTopActivity()
        }
    }

    /**
     * 获取前台并且可见的 Activity
     */
    fun getResumedActivity(): Activity? {
        return resumedActivity
    }

    /**
     * 判断当前应用是否处于前台状态
     */
    fun isForeground(): Boolean {
        return getResumedActivity() != null
    }

    /**
     * 注册应用生命周期回调
     */
    fun registerApplicationLifecycleCallback(callback: ApplicationLifecycleCallback) {
        lifecycleCallbacks.add(callback)
    }

    /**
     * 取消注册应用生命周期回调
     */
    fun unregisterApplicationLifecycleCallback(callback: ApplicationLifecycleCallback) {
        lifecycleCallbacks.remove(callback)
    }

    /**
     * 销毁指定的 Activity
     */
    fun finishActivity(clazz: Class<out Activity?>?) {
        if (clazz == null) {
            return
        }
        val keys: Array<String?> = activitySet.keys.toTypedArray()
        for (key: String? in keys) {
            val activity: Activity? = activitySet[key]
            if (activity == null || activity.isFinishing) {
                continue
            }
            if ((activity.javaClass == clazz)) {
                activity.finish()
                // activitySet.remove(key)
                break
            }
        }
    }

    /**
     * 销毁所有的 Activity
     */
    fun finishAllActivities() {
        finishAllActivities(null as Class<out Activity?>?)
    }

    /**
     * 销毁所有的 Activity
     *
     * @param classArray            白名单 Activity
     */
    @SafeVarargs
    fun finishAllActivities(vararg classArray: Class<out Activity>?) {
        val keys: Array<String?> = activitySet.keys.toTypedArray()
        for (key: String? in keys) {
            val activity: Activity? = activitySet[key]
            if (activity == null || activity.isFinishing) {
                continue
            }
            var whiteClazz = false
            for (clazz: Class<out Activity?>? in classArray) {
                if ((activity.javaClass == clazz)) {
                    whiteClazz = true
                }
            }
            if (whiteClazz) {
                continue
            }
            // 如果不是白名单上面的 Activity 就销毁掉
            activity.finish()
            // activitySet.remove(key)
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (AppConfig.isDebug()) Log.i(TAG, "onCreate - " + activity.javaClass.simpleName)
        callBack?.invoke(activity, STATUS_CREATED)
        if (activitySet.size == 0) {
            for (callback: ApplicationLifecycleCallback? in lifecycleCallbacks) {
                callback?.onApplicationCreate(activity)
            }
            if (AppConfig.isDebug()) Log.i(TAG, "onApplicationCreate - " + activity.javaClass.simpleName)
        }
        activitySet[getObjectTag(activity)] = activity
        topActivity = activity
    }

    override fun onActivityStarted(activity: Activity) {
        if (AppConfig.isDebug()) Log.i(TAG, "onStart - " + activity.javaClass.simpleName)
        callBack?.invoke(activity, STATUS_STARTED)
    }

    override fun onActivityResumed(activity: Activity) {
        if (AppConfig.isDebug()) Log.i(TAG, "onResume - " + activity.javaClass.simpleName)
        callBack?.invoke(activity, STATUS_RESUMED)
        if (topActivity === activity && resumedActivity == null) {
            for (callback: ApplicationLifecycleCallback in lifecycleCallbacks) {
                callback.onApplicationForeground(activity)
            }
            if (AppConfig.isDebug()) Log.i(TAG, "onApplicationForeground - " + activity.javaClass.simpleName)
        }
        topActivity = activity
        resumedActivity = activity
    }

    override fun onActivityPaused(activity: Activity) {
        if (AppConfig.isDebug()) Log.i(TAG, " onPause - " + activity.javaClass.simpleName)
        if (activity.isFinishing) {
            activity.intent.putExtra("pausedFinishingInvoked", true)
            callBack?.invoke(activity, STATUS_FINISHING)
        } else {
            callBack?.invoke(activity, STATUS_PAUSED)
        }
    }

    override fun onActivityStopped(activity: Activity) {
        if (AppConfig.isDebug()) Log.i(TAG, "onStop - " + activity.javaClass.simpleName)
        callBack?.invoke(activity, STATUS_STOPPED)
        if (resumedActivity === activity) {
            resumedActivity = null
        }
        if (resumedActivity == null) {
            for (callback: ApplicationLifecycleCallback in lifecycleCallbacks) {
                callback.onApplicationBackground(activity)
            }
            if (AppConfig.isDebug()) Log.i(TAG, "onApplicationBackground - " + activity.javaClass.simpleName)
        }
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        if (AppConfig.isDebug()) Log.i(TAG, "onSaveInstanceState - " + activity.javaClass.simpleName)
    }

    override fun onActivityDestroyed(activity: Activity) {
        if (AppConfig.isDebug()) Log.i(TAG, "onDestroy - " + activity.javaClass.simpleName)
        if (!activity.intent.getBooleanExtra("pausedFinishingInvoked", false)) {
            callBack?.invoke(activity, STATUS_FINISHING)
        } else {
            callBack?.invoke(activity, STATUS_DESTROYED)
        }
        activitySet.remove(getObjectTag(activity))
        if (topActivity === activity) {
            topActivity = null
        }
        if (activitySet.size == 0) {
            for (callback: ApplicationLifecycleCallback in lifecycleCallbacks) {
                callback.onApplicationDestroy(activity)
            }
            if (AppConfig.isDebug()) Log.i(TAG, "onApplicationDestroy - " + activity.javaClass.simpleName)
        }
    }

    /**
     * 应用生命周期回调
     */
    interface ApplicationLifecycleCallback {
        /**
         * 第一个 Activity 创建了
         */
        fun onApplicationCreate(activity: Activity)

        /**
         * 最后一个 Activity 销毁了
         */
        fun onApplicationDestroy(activity: Activity)

        /**
         * 应用从前台进入到后台
         */
        fun onApplicationBackground(activity: Activity)

        /**
         * 应用从后台进入到前台
         */
        fun onApplicationForeground(activity: Activity)
    }
}