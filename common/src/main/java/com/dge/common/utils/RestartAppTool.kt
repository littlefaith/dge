package com.dge.common.utils

import android.app.ActivityManager
import android.content.Context
import android.os.Process
import com.dge.common.manager.ActivityStackManager
import kotlin.system.exitProcess

/**
 * @author Jasper Jiao
 */
object RestartAppTool {
    /**
     * 杀死APP进程
     */
    fun exitAPP(context: Context) {
        ActivityStackManager.getInstance().finishAllActivities()
        //注意：不能先杀掉主进程，否则逻辑代码无法继续执行，需先杀掉相关进程最后杀掉主进程
        val mActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val mList = mActivityManager.runningAppProcesses
        if (mList != null) {
            for (runningAppProcessInfo in mList) {
                if (runningAppProcessInfo.pid != Process.myPid()) {
                    Process.killProcess(runningAppProcessInfo.pid)
                }
            }
        }
        Process.killProcess(Process.myPid())
        exitProcess(0)
    }
}