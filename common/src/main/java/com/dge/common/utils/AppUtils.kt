package com.dge.common.utils

import com.dge.common.common.UserInfoMgr

object AppUtils {
    fun logout() {
        UserInfoMgr.logout()

        System.gc()
//        UserLoginMgr.instance.mContext?.cactusUnregister()
    }
}