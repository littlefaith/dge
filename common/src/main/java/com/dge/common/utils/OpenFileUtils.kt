package com.dge.common.utils

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import com.dge.common.LibApp
import com.dge.common.common.AppConfig.isDebug
import com.dge.common.common.Constants
import com.dge.common.utils.FileUtil.getMimeType
import timber.log.Timber
import java.io.File
import java.util.*

object OpenFileUtils {
    fun getOpenFileIntent2(filePath: String): Intent? {
        val file = File(filePath)
        if (!file.exists()) return null
        /* 取得扩展名 */
        val end = file.name.substring(file.name.lastIndexOf(".") + 1, file.name.length).lowercase(
            Locale.getDefault()
        )
        return when (end) {
            "m4a", "mp3", "mid", "xmf", "ogg", "wav" -> getAudioFileIntent(filePath)
            "3gp", "mp4" -> getVideoFileIntent(filePath)
            "jpg", "gif", "png", "jpeg", "bmp" -> getImageFileIntent(filePath)
            "apk" -> getApkFileIntent(filePath)
            "ppt" -> getPptFileIntent(filePath)
            "xls", "xlsx" -> getExcelFileIntent(filePath)
            "doc", "docx" -> getWordFileIntent(filePath)
            "pdf" -> getPdfFileIntent(filePath)
            "chm" -> getChmFileIntent(filePath)
            "txt" -> getTextFileIntent(filePath, false)
            else -> getAllIntent(filePath)
        }
    }

    //Android获取一个用于打开APK文件的intent
    private fun getAllIntent(param: String): Intent {
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(getUriFromFile(File(param), intent), "*/*")
        return intent
    }

    //Android获取一个用于打开APK文件的intent
    private fun getApkFileIntent(param: String): Intent {
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/vnd.android.package-archive")
        return intent
    }

    //Android获取一个用于打开VIDEO文件的intent
    private fun getVideoFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("oneshot", 0)
        intent.putExtra("configchange", 0)
        intent.setDataAndType(getUriFromFile(File(param), intent), "video/*")
        return intent
    }

    //Android获取一个用于打开AUDIO文件的intent
    private fun getAudioFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("oneshot", 0)
        intent.putExtra("configchange", 0)
        intent.setDataAndType(getUriFromFile(File(param), intent), "audio/*")
        return intent
    }

    //Android获取一个用于打开Html文件的intent
    fun getHtmlFileIntent(param: String): Intent {
        val uri = Uri.parse(param).buildUpon().encodedAuthority("com.android.htmlfileprovider").scheme("content")
            .encodedPath(param).build()
        val intent = Intent("android.intent.action.VIEW")
        intent.setDataAndType(uri, "text/html")
        return intent
    }

    //Android获取一个用于打开图片文件的intent
    private fun getImageFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "image/*")
        return intent
    }

    //Android获取一个用于打开PPT文件的intent
    private fun getPptFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/vnd.ms-powerpoint")
        return intent
    }

    //Android获取一个用于打开Excel文件的intent
    private fun getExcelFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/vnd.ms-excel")
        return intent
    }

    //Android获取一个用于打开Word文件的intent
    private fun getWordFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/msword")
        return intent
    }

    //Android获取一个用于打开CHM文件的intent
    private fun getChmFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/x-chm")
        return intent
    }

    //Android获取一个用于打开文本文件的intent
    private fun getTextFileIntent(param: String, paramBoolean: Boolean): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (paramBoolean) {
            val uri1 = Uri.parse(param)
            intent.setDataAndType(uri1, "text/plain")
        } else {
            intent.setDataAndType(getUriFromFile(File(param), intent), "text/plain")
        }
        return intent
    }

    //Android获取一个用于打开PDF文件的intent
    private fun getPdfFileIntent(param: String): Intent {
        val intent = Intent("android.intent.action.VIEW")
        intent.addCategory("android.intent.category.DEFAULT")
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setDataAndType(getUriFromFile(File(param), intent), "application/pdf")
        return intent
    }

    fun getOpenFileIntent(filePath: String): Intent? {
        val file = File(filePath)
        if (!file.exists()) return null
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        //设置intent的Action属性
        intent.action = Intent.ACTION_VIEW
        //获取文件file的MIME类型
        val type = getMimeType(filePath)
        val uri = getUriFromFile(file, intent)
        if (isDebug()) {
            Timber.i("type = %s，uri = %s", type, uri.toString())
        }
        //设置intent的data和Type属性。
        intent.setDataAndType(uri, type)
        return intent
    }

    private fun getUriFromFile(file: File, intent: Intent): Uri {
        var uri: Uri
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                uri = FileProvider.getUriForFile(LibApp.context, Constants.fileProviderAuthority, file)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            } catch (e: Exception) {
                uri = Uri.fromFile(file)
            }
        } else {
            uri = Uri.fromFile(file)
        }
        return uri
    }
}