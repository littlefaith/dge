package com.dge.common.utils

import android.content.Intent
import android.text.TextUtils
import com.luck.picture.lib.basic.PictureSelector
import com.luck.picture.lib.entity.LocalMedia

/**
 * @author Jasper Jiao
 * @time 2022/3/24
 * @des 获取pictureSelector选择的图片的路径
 */
object PathUtils {
    fun getPath(media: LocalMedia): String {
        return if (media.isCut && !media.isCompressed) {
            // 裁剪过
            media.cutPath ?: ""
        } else if (media.isCompressed) {
            // 压缩过，最终已压缩图片为准
            media.compressPath ?: ""
        } else {
            // 原图
            media.path ?: ""
        }
    }

    /**
     * 获取从PictureSelector中得到的第一张图片的路径
     */
    fun obtainFirstImage(data: Intent?): String {
        if (data == null) return ""
        val list = PictureSelector.obtainSelectorList(data)
        if (list.isNullOrEmpty()) return ""
        val localMedia = list[0]
        if (localMedia == null || TextUtils.isEmpty(localMedia.path)) {
            return ""
        }
        return getPath(localMedia)
    }
}