package com.dge.common.bean

data class BaseBean(
    val code: Int,
    val `data`: Any,
    val msg: String,
    val message: String,
    val success: Boolean // true
)