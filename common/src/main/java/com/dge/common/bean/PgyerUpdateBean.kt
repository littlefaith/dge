package com.dge.common.bean

data class PgyerUpdateBean(
    val code: Int, // 0
    val `data`: Data,
    val message: String
) {
    data class Data(
        val buildBuildVersion: String, // 4
        val buildCreated: String, // 2021-10-28 09:04:31
        val buildFileName: String, // queue-0.0.4-release.apk
        val buildFileSize: String, // 6669872
        val buildIdentifier: String, //
        val buildIsFirst: String, // 0
        val buildIsLastest: String, // 1
        val buildKey: String, //
        val buildName: String, //
        val buildScreenshots: String,
        val buildShortcutUrl: String, // O56n
        val buildType: String, // 2
        val buildUpdateDescription: String,
        val buildUpdated: String, // 2021-10-28 09:04:31
        val buildVersion: String, // 0.0.4
        val buildVersionNo: String // 202110272
    )
}