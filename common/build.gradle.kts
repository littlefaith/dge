@file:Suppress("UnstableApiUsage")

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize") //@Parcelize序列化标注类
    id("io.michaelrocks.paranoid")
}
apply(from = "${rootDir.path}/common.gradle")
android {
    namespace = "com.dge.common"
    defaultConfig {
        // testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
        externalNativeBuild {
            cmake {
                cppFlags("")
            }
        }
        resConfigs(listOf("zh", "xxhdpi"))// 仅保留中文语种的资源，仅保留xxhdpi 图片资源（目前主流分辨率 1920 * 1080）
        ndk {
            abiFilters += listOf("armeabi-v7a", "arm64-v8a")
        }
        buildFeatures {
            viewBinding = true // 使用viewBinding
        }
    }
    externalNativeBuild {
        cmake {
            path("CMakeLists.txt")
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    paranoid {
    }
}

dependencies {
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    api("androidx.core:core-ktx:1.9.0")
    api("androidx.activity:activity-ktx:1.6.1")
    api("androidx.constraintlayout:constraintlayout:2.1.4")
    api("androidx.fragment:fragment-ktx:1.5.7")
    // recyclerview
    api("androidx.recyclerview:recyclerview:1.3.1")
    // Gson 解析容错：https://github.com/getActivity/GsonFactory
    implementation("com.github.getActivity:GsonFactory:8.0")
    // Json 解析框架：https://github.com/google/gson
    implementation("com.google.code.gson:gson:2.10.1")
    // 网络框架
    api("com.squareup.okhttp3:okhttp:5.0.0-alpha.2")
    api("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.2")
    api("com.squareup.okhttp3:okhttp-urlconnection:5.0.0-alpha.2")
    // 强大的弹窗库:https://github.com/li-xiaojun/XPopup
    api("com.github.li-xiaojun:XPopup:2.9.19")
    // 权限请求框架：https://github.com/getActivity/XXPermissions
    api("com.github.getActivity:XXPermissions:18.2")
    // 设置状态栏和导航栏的各种效果的框架：https://github.com/Zackratos/UltimateBarX
    api("com.gitee.zackratos:UltimateBarX:0.8.0")
    // 防混淆配置(不需要被其它模块引入，所以用implementation):https://github.com/Blankj/FreeProGuard
    implementation("com.blankj:free-proguard:1.0.2")
    // ViewBinding 封装：https://github.com/DylanCaiCoding/ViewBindingKtx
    api("com.github.DylanCaiCoding.ViewBindingKTX:viewbinding-ktx:2.1.0")
    api("com.github.DylanCaiCoding.ViewBindingKTX:viewbinding-base:2.1.0")
    // api("com.github.DylanCaiCoding.ViewBindingKTX:viewbinding-brvah:2.1.0") {
    //     exclude("com.github.CymChad", "BaseRecyclerViewAdapterHelper")
    // }
    // 一个强大并且灵活的RecyclerViewAdapter: https://github.com/CymChad/BaseRecyclerViewAdapterHelper
    api("io.github.cymchad:BaseRecyclerViewAdapterHelper:4.0.3")
    // 对APP进行组件化改造的路由框架:http://therouter.cn/
    // api("cn.therouter:router:1.1.2")
    // kapt("cn.therouter:apt:1.1.2")
    // glide 图片框架
    api("com.github.bumptech.glide:glide:4.15.1")
    kapt("com.github.bumptech.glide:compiler:4.15.1")
    // ActionBar：https://github.com/getActivity/TitleBar
    api("com.github.getActivity:TitleBar:10.5")
    // 本地异常捕捉框架：https://github.com/Ereza/CustomActivityOnCrash
    api("cat.ereza:customactivityoncrash:2.4.0")
    // eventbus
    api("org.greenrobot:eventbus:3.3.1")
    // 图片压缩升级库，基于Luban：https://github.com/forJrking/KLuban
    api("com.github.forJrking:KLuban:1.1.0")
    // AndroidUtilCode工具类：https://github.com/Blankj/AndroidUtilCode
    api("com.blankj:utilcodex:1.31.1")
    // jetpack lifecycle
    api("androidx.lifecycle:lifecycle-extensions:2.2.0")

    api("com.lxj:statelayout:1.2.2")

    api("com.google.android:flexbox:2.0.1")
    // 动画解析库：https://github.com/airbnb/lottie-android
    // 动画资源：https://lottiefiles.com、https://icons8.com/animated-icons
    api("com.airbnb.android:lottie:5.2.0")
    // 图片选择器：https://github.com/LuckSiege/PictureSelector
    api("io.github.lucksiege:pictureselector:v3.10.8")
    // 图片裁剪：https://github.com/LuckSiege/uCrop
    api("io.github.lucksiege:ucrop:v3.10.8")
    api("androidx.legacy:legacy-support-v4:1.0.0")
    // ShapeView：https://github.com/getActivity/ShapeView
    api("com.github.getActivity:ShapeView:9.0")
    // ShapeDrawable：https://github.com/getActivity/ShapeDrawable
    api("com.github.getActivity:ShapeDrawable:3.0")
    // 轮播图：https://github.com/youth5201314/banner
    api("io.github.youth5201314:banner:2.2.2")
    // ui组件模块
    api(project(":lib-widget"))
    // AOP 插件库：https://mvnrepository.com/artifact/org.aspectj/aspectjrt
    api("org.aspectj:aspectjrt:1.9.9.1")
    // 日志打印框架：https://github.com/JakeWharton/timber
    api("com.jakewharton.timber:timber:5.0.1")
    // 吐司框架：https://github.com/getActivity/ToastUtils
    api("com.github.getActivity:ToastUtils:11.2")
    // 腾讯 MMKV：https://github.com/Tencent/MMKV
    api("com.tencent:mmkv-static:1.2.15")
    //屏幕适配：https://github.com/JessYanCoding/AndroidAutoSize
    implementation("com.github.JessYanCoding:AndroidAutoSize:v1.2.1")
    // 一个TextView可设置的效果:https://github.com/zrq1060/SpanBuilder
    api("io.github.zrq1060:spans:1.1.0")
}