plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}
apply {
    from("${rootDir.path}/common.gradle")
}
android {
    namespace = "com.dge.libwidget"
    defaultConfig {
        // testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    // testImplementation("junit:junit:4.13.2")
    implementation("androidx.core:core-ktx:1.9.0")
    // api (project(":lib-widget:shape_view"))
    api(project(":lib-widget:common_widget"))
}