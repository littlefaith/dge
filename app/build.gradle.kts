@file: Suppress("UnstableApiUsage")

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize") //@Parcelize序列化标注类
    id("io.github.wurensen.android-aspectjx")
    id("io.michaelrocks.paranoid")
    id("AndResGuard")
    // id("therouter") //一般项目app中引用就够了
}
apply {
    from("${rootDir.path}/common.gradle")
    from("${rootDir.path}/and_res_guard.gradle")
}
android {
    namespace = "com.dge.app"
    defaultConfig {
        applicationId = "com.dge.app"
        versionCode = 100
        versionName = "0.0.1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        resConfigs(listOf("zh", "xxhdpi"))// 仅保留中文语种的资源，仅保留xxhdpi 图片资源（目前主流分辨率 1920 * 1080）
        ndk {
            abiFilters += listOf("armeabi-v7a", "arm64-v8a")
        }
        buildFeatures {
            viewBinding = true // 使用viewBinding
        }
    }
    signingConfigs {
        create("signConfig") {
            storeFile = file("${rootDir.path}/key-store.jks")
            storePassword = "edgkey231113"
            keyAlias = "edg"
            keyPassword = "edgkey231113"
        }
    }
    buildTypes {
        named("debug") {
            // 压缩对齐开关
            isZipAlignEnabled = true
            // 移除无用的资源
            isShrinkResources = false
            // 代码混淆开关
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-project.txt")
            signingConfig = signingConfigs.getByName("signConfig")
            lintOptions {
                isCheckDependencies = true // 增量Lint分析任务，减少构建时间
                isCheckReleaseBuilds = false
                isAbortOnError = false
            }
            isDebuggable = true
            isJniDebuggable = true
            // 添加清单占位符
            manifestPlaceholders += mapOf(
                "APP_NAME" to "DGE Debug"
            )
            ndk {
                abiFilters += listOf("armeabi-v7a", "arm64-v8a")
            }
        }
        named("release") {
            // 显示Log
            // buildConfigField("boolean", "LOG_DEBUG", "true")
            // Zipalign优化
            isZipAlignEnabled = true
            // 移除无用的resource文件
            isShrinkResources = true
            // 混淆
            isMinifyEnabled = true
            // 加载默认混淆配置文件
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            // 签名
            signingConfig = signingConfigs.getByName("signConfig")
            lintOptions {
                isCheckDependencies = true // 增量Lint分析任务，减少构建时间
                isCheckReleaseBuilds = false
                isAbortOnError = false
            }
            isDebuggable = false
            isJniDebuggable = false
            manifestPlaceholders += mapOf(
                "APP_NAME" to "DGE"
            )
            ndk {
                abiFilters += listOf("armeabi-v7a", "arm64-v8a")
            }
        }
    }

    packagingOptions {
        resources {
            excludes.add("META-INF/library_release.kotlin_module")
            excludes.add("AndroidManifest.xml")
            // pickFirsts.add("")
        }
    }

    applicationVariants.configureEach {
        outputs.configureEach {
            if (this is com.android.build.gradle.internal.api.ApkVariantOutputImpl) {
                if (this.outputFile != null && this.outputFile.name.endsWith(".apk")) {
                    this.outputFileName = "V${defaultConfig.versionName}-${this.baseName}.apk"
                }
            }
        }
    }
    //此处添加你要过滤的文件
    // configurations {
    //     configureEach {
    //         exclude(group = "com.google.guava", module = "listenablefuture")
    //     }
    // }
    // ndkVersion("21.4.7075529") // 目前无需指定ndk的版本
    // AOP 配置（exclude 和 include 二选一）
    // 需要进行配置，否则就会引发冲突，具体表现为：
    // 第一种：编译不过去，报错：java.util.zip.ZipException：Cause: zip file is empty
    // 第二种：编译能过去，但运行时报错：ClassNotFoundException: Didn't find class on path: DexPathList
    aspectjx {
        // 排除一些第三方库的包名（Gson、 LeakCanary 和 AOP 有冲突）
        // exclude 'androidx', 'com.google', 'com.squareup', 'org.apache', 'com.alipay', 'com.taobao', 'versions.9'
        // 只对以下包名做 AOP 处理
        include(android.defaultConfig.applicationId!!, "com.dge.common")
        // ,"com.dge.common")
    }

    paranoid {
        // enabled = true //default is true
        // cacheable = true // Allows to enable caching for the transform. Default value is false
        // includeSubprojects = true // Allows to enable obfuscation for subprojects. Default value is false.
    }
}

dependencies {
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    // kapt("cn.therouter:apt:1.1.2")
    //公共模块
    implementation(project(":common"))
}