package com.dge.app.adapters

import android.content.Context
import android.view.ViewGroup
import android.webkit.WebSettings
import androidx.appcompat.app.AppCompatActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.dragswipe.listener.DragAndSwipeDataCallback
import com.chad.library.adapter.base.viewholder.QuickViewHolder
import com.dge.app.R
import com.dge.common.common.URLConstants
import com.dge.common.extentions.color
import com.dge.common.widgets.view.BrowserView
import timber.log.Timber

class StatisticPanelAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, QuickViewHolder>(data), DragAndSwipeDataCallback {
    val statisticNames = arrayListOf<String>()
    override fun onBindViewHolder(holder: QuickViewHolder, position: Int, item: String?) {
        if (item == null) {
            //default view
        } else {
            if (position < statisticNames.size) {
                holder.setGone(R.id.tvTitle, false)
                holder.setText(R.id.tvTitle, statisticNames[position])
            } else {
                holder.setGone(R.id.tvTitle, true)
            }
            val webView = holder.getView<BrowserView>(R.id.webView)
            if (context is AppCompatActivity) {
                webView.setLifecycleOwner(context as AppCompatActivity)
            }
            webView.settings.databaseEnabled = true
            webView.settings.useWideViewPort = true
            webView.settings.userAgentString = webView.settings.userAgentString + "android"
            webView.settings.setRenderPriority(WebSettings.RenderPriority.HIGH)
            webView.settings.setSupportZoom(false)
            webView.setBackgroundColor(R.color.colorWhiteEEEEEE.color())
            webView.settings.blockNetworkImage = false //解决图片不显示
            webView.setBrowserViewClient(BrowserView.BrowserViewClient(webView))
            webView.setBrowserChromeClient(BrowserView.BrowserChromeClient(webView))
            val url = URLConstants.URL_USER_BASE + item
            Timber.i(url)
            webView.loadUrl(url)
        }
    }

    override fun onCreateViewHolder(context: Context, parent: ViewGroup, viewType: Int): QuickViewHolder {
        return QuickViewHolder(R.layout.item_statistics_web, parent)
    }

    fun setStatisticNames(names: ArrayList<String>) {
        statisticNames.clear()
        statisticNames.addAll(names)
    }

    override fun dataMove(fromPosition: Int, toPosition: Int) {
        move(fromPosition, toPosition)
    }

    override fun dataRemoveAt(position: Int) {
        removeAt(position)
    }
}