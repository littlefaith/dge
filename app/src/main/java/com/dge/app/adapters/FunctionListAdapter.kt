package com.dge.app.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.dragswipe.listener.DragAndSwipeDataCallback
import com.chad.library.adapter.base.viewholder.QuickViewHolder
import com.dge.app.R
import com.dge.app.utils.ResourceUtils
import com.dge.common.extentions.dp
import com.dge.common.extentions.drawable
import com.dge.common.extentions.expand

class FunctionListAdapter(private val data: MutableList<String>) :
    BaseQuickAdapter<String, QuickViewHolder>(data), DragAndSwipeDataCallback {
    override fun onBindViewHolder(holder: QuickViewHolder, position: Int, item: String?) {
        if (item == null) {
            //default view
        } else {
            val isAdd = item == "更多" && itemCount < 10
            val tvTitle = holder.getView<TextView>(R.id.tvTitle)
            if (!isAdd) tvTitle.text = item
            val drawable =
                if (isAdd) R.mipmap.ic_add.drawable() else ResourceUtils.nameResourceMap[item]?.drawable()
            drawable?.setBounds(0, 0, 40.dp, 40.dp)
            tvTitle.setCompoundDrawables(null, drawable, null, null)
            holder.getView<View>(R.id.ivDelete).expand(3.dp, 3.dp)
        }
    }

    override fun onCreateViewHolder(context: Context, parent: ViewGroup, viewType: Int): QuickViewHolder {
        return QuickViewHolder(R.layout.item_function, parent)
    }

    override fun dataMove(fromPosition: Int, toPosition: Int) {
        move(fromPosition, toPosition)
    }

    override fun dataRemoveAt(position: Int) {
        removeAt(position)
    }
}