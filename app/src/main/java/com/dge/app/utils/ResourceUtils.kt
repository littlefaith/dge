package com.dge.app.utils

import com.dge.app.R

/**
 * @auther Jasper Jiao
 * @time 2023/11/14 19:38
 * @des 资源工具类
 */
object ResourceUtils {
    val nameResourceMap = hashMapOf(
        "一场仗" to R.mipmap.ic_a_battle,
        "比学赶超" to R.mipmap.ic_competition,
        "超时亮灯" to R.mipmap.ic_timeout_light,
        "成果晾晒" to R.mipmap.ic_result_show,
        "督办圈阅" to R.mipmap.ic_supervise_and_read,
        "揭榜挂帅" to R.mipmap.ic_rank,
        "目标任务" to R.mipmap.ic_target_task,
        "一表通" to R.mipmap.ic_sheet,
        "重点项目" to R.mipmap.ic_important_project,
        "目标计划" to R.mipmap.ic_fun_ji,
        "部门账簿" to R.mipmap.ic_fun_bu,
        "流通备案" to R.mipmap.ic_fun_bei,
        "共享超市" to R.mipmap.ic_fun_xiang,
        "开放超市" to R.mipmap.ic_fun_fang,
        "流通考核" to R.mipmap.ic_fun_kao,
        "资源画像" to R.mipmap.ic_fun_hua,
        "资源云行" to R.mipmap.ic_fun_yun,
        "更多" to R.mipmap.ic_fun_more,
    )
}