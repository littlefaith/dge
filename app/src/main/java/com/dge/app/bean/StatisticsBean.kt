package com.dge.app.bean

import androidx.annotation.Keep

@Keep
data class StatisticsBean(
    val names: ArrayList<String> = arrayListOf(),
    val urls: ArrayList<String> = arrayListOf()
)