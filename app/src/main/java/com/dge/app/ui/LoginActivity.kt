package com.dge.app.ui

import android.os.Bundle
import com.dge.app.R
import com.dge.app.databinding.ActivityLoginBinding
import com.dge.common.base.view.BaseActivity
import com.dge.common.common.Constants
import com.dge.common.common.UserInfoMgr
import com.dge.common.extentions.mmkv
import com.dge.common.extentions.singleClick
import com.dge.common.utils.Utils

class LoginActivity : BaseActivity<ActivityLoginBinding>() {
    private var isRememberAccountPwd = false
    override fun onCreate(savedInstanceState: Bundle?) {
        isSetStatusBarColor = false
        super.onCreate(savedInstanceState)
        Utils.setImmersiveStatusBar(this, R.color.transparent, false, false)
        initData()
        initListeners()
    }

    private fun initData() {
        isRememberAccountPwd = mmkv().getBoolean(Constants.PREF_KEY_IS_REMEMBER_LOGIN_INFO, false)
        if (isRememberAccountPwd) {
            binding.edtAccount.setText(UserInfoMgr.account)
            binding.ivSavePwd.setImageResource(R.mipmap.checkbox_checked)
        } else {
            binding.ivSavePwd.setImageResource(R.mipmap.checkbox_normal)
        }
    }

    private fun initListeners() {
        binding.ivSavePwd.singleClick {
            if (isRememberAccountPwd) {
                binding.ivSavePwd.setImageResource(R.mipmap.checkbox_normal)
            } else {
                binding.ivSavePwd.setImageResource(R.mipmap.checkbox_checked)
            }
            isRememberAccountPwd = !isRememberAccountPwd
        }
        binding.btnLogin.singleClick {
            //
            // val account = binding.edtAccount.text.toString()
            // val pwd = binding.edtPassword.text.toString()
            // if (TextUtils.isEmpty(account) || TextUtils.isEmpty(pwd)) {
            //     showToastShort("账号或密码不能为空")
            //     return@singleClick
            // }
            startActivity(MainActivity::class.java)
            // TheRouter.build(Constants.ACTIVITY_URL_MAIN).navigation()
            finish()
        }
    }
}