package com.dge.app.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.dge.app.R
import com.dge.app.databinding.ActivitySplashBinding
import com.dge.common.base.view.BaseActivity
import com.dge.common.common.AppConfig
import com.dge.common.common.URLConstants
import com.dge.common.extentions.visible
import com.dge.common.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@SuppressLint("CustomSplashScreen")
// @Route(path = Constants.ACTIVITY_URL_SPLASH)
class SplashActivity : BaseActivity<ActivitySplashBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        isSetStatusBarColor = false
        super.onCreate(savedInstanceState)
        Utils.setImmersiveStatusBar(this, R.color.transparent, false, false)
        setEnvTag()
        init()
    }

    private fun barTransparent() {
        val window = window
        window.statusBarColor = Color.TRANSPARENT
        window.navigationBarColor = Color.TRANSPARENT
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    // @Permissions(0, Permission.MANAGE_EXTERNAL_STORAGE)
    private fun init() {
        launch {
            withContext(Dispatchers.IO) {
                delay(1500)
                startActivity(MainActivity::class.java)
                // val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                // startActivity(intent)
                finish()
            }
        }
    }

    private fun setEnvTag() {
        if (AppConfig.isDebug()) {
            binding.ivInterfaceEnv.visible()
            if (URLConstants.IS_PUBLISH_VERSION) {
                binding.ivInterfaceEnv.setText("正式环境")
            } else
                binding.ivInterfaceEnv.setText("测试环境")
        }
    }
}