buildscript {
    dependencies {
        classpath("com.android.tools.build:gradle:7.4.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.0")
        // AOP 配置插件：https://github.com/wurensen/gradle_plugin_android_aspectjx
        classpath("io.github.wurensen:gradle-android-plugin-aspectjx:3.3.2")
        // 字符串保护框架：https://github.com/MichaelRocks/paranoid
        classpath("io.michaelrocks:paranoid-gradle-plugin:0.3.7")
        //资源混淆框架：https://github.com/Leon406/AndResGuard
        classpath("io.github.leon406:AndResGuard-gradle-plugin:1.2.23")
    }
}

plugins {
    id("com.android.application") version "7.4.2" apply false
    id("com.android.library") version "7.4.2" apply false
    id("org.jetbrains.kotlin.android") version "1.8.0" apply false
    // id("cn.therouter") version "1.1.2" apply false
}

allprojects {
    tasks.withType(JavaCompile::class.java).configureEach {
        // 设置全局编码
        options.encoding = "UTF-8"
    }
    // tasks.withType(Javadoc::class.java).configureEach {
    //     //  设置文档编码
    //     options {
    //         encoding = "UTF-8"
    //         charSet = "UTF-8"
    //         links("http://docs.oracle.com/javase/7/docs/api")
    //     }
    // }
}

tasks.register("clean", Delete::class.java) {
    delete(rootProject.buildDir)
}