pluginManagement { //原buildscript
    repositories {
        gradlePluginPortal()
        maven("https://maven.aliyun.com/repository/releases")
        maven("https://maven.aliyun.com/repository/central") //central仓库，某些情况下public中的central仓库更新会不及时，所以加上这个
        maven("https://maven.aliyun.com/repository/public") //central仓和jcenter仓的聚合仓
        maven("https://maven.aliyun.com/repository/google")
        maven("https://maven.aliyun.com/repository/gradle-plugin")
        google()
    }
}
dependencyResolutionManagement {//原：allprojects
// By default, repositories declared by a project will override whatever is declared in settings. You can change this behavior to make sure that you always use the settings repositories:
// repositoriesMode.set(RepositoriesMode.PREFER_SETTINGS)
// If, for some reason, a project or a plugin declares a repository in a project, Gradle would warn you. You can however make it fail the build if you want to enforce that only settings repositories are used
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven("https://maven.aliyun.com/repository/releases")
        maven("https://maven.aliyun.com/repository/central") //central仓库，某些情况下public中的central仓库更新会不及时，所以加上这个
        maven("https://maven.aliyun.com/repository/public") //central仓和jcenter仓的聚合仓
        maven("https://maven.aliyun.com/repository/google")
        maven("https://jitpack.io")
        google()
    }
}

include("common")
include("app")
include("lib-widget")
include("lib-widget:common_widget")
